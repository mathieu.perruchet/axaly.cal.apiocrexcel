﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;

namespace myApp
{
    public class Program
    {
        //log configuration
        private static Logger log = LogManager.LoadConfiguration("Nlog.config").GetLogger("file");
        private static Logger journal = LogManager.LoadConfiguration("Nlog.config").GetLogger("journal");

        //nom de la feuille excel
        private static string feuilleExcel = ReadSetting("feuilleExcel");

        //url api
        private static string urlApi = ReadSetting("lienApi");

        //file and repository path
        private static string pathExcel = ReadSetting("pathExcel"); //chemin du fichier excel
        private static string pathExcelCopy = ReadSetting("pathExcelCopy"); //chemin du repertoire des save excel
        private static string nameExcelCopy = ReadSetting("nameExcelCopy"); //nom du fichier excel save

        private static string pathRepo = ReadSetting("pathRepo");
        private static string pathRepoMovedFile = ReadSetting("pathRepoMovedFile");

        //proxy configuration
        private static string useProxy = ReadSetting("useProxy");

        private static string proxyHost = ReadSetting("proxyHost");
        private static string proxyPort = ReadSetting("proxyPort");
        private static string proxyUserName = ReadSetting("proxyUserName");
        private static string proxyPassword = ReadSetting("proxyPassword");

        //http client
        private static HttpClient client;

        private static async Task ProcessRepositories()
        {
            // récupère tous les fichiers dans le dossier
            try
            {
                log.Info("Tentative d'accès au dossier des fichiers à traiter (" + pathRepo + ")");
                string[] filesInRepo = Directory.GetFiles(pathRepo);
                log.Info("Accès réussi");
                //recupère la date et l'heure du jour
                String dateString = getDate();
                try
                {
                    using (FileStream fs = new FileStream(pathExcel, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                    {
                        try 
                        { 
                            using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, true))
                            {
                                // récupère la feuille voulue
                                WorkbookPart workbookPart = doc.WorkbookPart;

                                WorksheetPart worksheetPart = GetWorksheetPartByName(workbookPart, feuilleExcel);
                                Worksheet sheet = worksheetPart.Worksheet;

                                SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                                
                                // var cells = sheet.Descendants<Cell>();
                                var rows = sheet.Descendants<Row>();

                                uint nbRowBase = (uint)rows.LongCount();

                                uint nbRow = (uint)rows.LongCount();
                                nbRow += 2; // pour commencer juste apres la derniere ligne

                                int nbEntree = 0;

                                foreach (string aFile in filesInRepo)
                                {
                                    // récupère le streamContent de chaque fichier dans le dossier
                                    FileStream fileStream = System.IO.File.OpenRead(aFile);
                                    StreamContent fileStreamContent = new StreamContent(fileStream);

                                    client.DefaultRequestHeaders.Accept.Clear();
                                    client.DefaultRequestHeaders.Accept.Add(
                                        new MediaTypeWithQualityHeaderValue("application/json"));

                                    var content = new MultipartFormDataContent();

                                    content.Add(fileStreamContent, "file1", aFile);
                                    var response = await client.PostAsync(urlApi, content);

                                    if (response.IsSuccessStatusCode) // on vérifie sur la requête Http a généré un code retour de succès (200, 201, ...)
                                    {
                                        var responseData = await response.Content.ReadAsStringAsync(); // récupération du contenu de la réponse Http

                                        var repositories = JsonSerializer.Deserialize<List<Repository>>(responseData); // desérialisation du contenu

                                        foreach (var repo in repositories)
                                        {
                                            // Create new row
                                            Row row = new Row { RowIndex = nbRow };

                                            DateTime dP = DateTime.ParseExact(repo.DatePiece, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            DateTime dV = DateTime.ParseExact(repo.DateValeur, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                            DateTime dO = DateTime.ParseExact(repo.DateOp, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                            double montantFloat = Math.Round(float.Parse(repo.Montant),2);
                                           

                                            // Create new cell
                                            //Cell cell1 = new Cell { CellReference = "A" + nbRow, DataType = CellValues.Number, CellValue = new CellValue(datePiece) };
                                            Cell cell1 = new Cell { CellReference = "A" + nbRow, StyleIndex = 1, CellValue = new CellValue(dP.ToOADate().ToString()) };
                                            Cell cell2 = new Cell { CellReference = "B" + nbRow, DataType = CellValues.String, CellValue = new CellValue(repo.Societe) };
                                            Cell cell3 = new Cell { CellReference = "C" + nbRow, DataType = CellValues.String, CellValue = new CellValue(repo.CompteBenef) };
                                            Cell cell4 = new Cell { CellReference = "D" + nbRow, DataType = CellValues.String, CellValue = new CellValue(repo.ModeRglt) };
                                            //Cell cell5 = new Cell { CellReference = "E" + nbRow, DataType = CellValues.Number, CellValue = new CellValue(dateOp) };
                                            Cell cell5 = new Cell { CellReference = "E" + nbRow, StyleIndex = 1, CellValue = new CellValue(dO.ToOADate().ToString()) };
                                            //Cell cell6 = new Cell { CellReference = "F" + nbRow, DataType = CellValues.Number, CellValue = new CellValue(dateValeur) };
                                            Cell cell6 = new Cell { CellReference = "F" + nbRow, StyleIndex = 1, CellValue = new CellValue(dV.ToOADate().ToString()) };
                                            Cell cell7 = new Cell { CellReference = "G" + nbRow, DataType = CellValues.String, CellValue = new CellValue(repo.NumCheq) };
                                            Cell cell8 = new Cell { CellReference = "H" + nbRow, DataType = CellValues.Number, CellValue = new CellValue(montantFloat) };
                                            Cell cell9 = new Cell { CellReference = "I" + nbRow, DataType = CellValues.String, CellValue = new CellValue(repo.Banque) };
                                            Cell cell10 = new Cell { CellReference = "J" + nbRow, DataType = CellValues.String, CellValue = new CellValue(repo.Guichet) };
                                            Cell cell11 = new Cell { CellReference = "K" + nbRow, DataType = CellValues.String, CellValue = new CellValue(repo.Compte) };
                                            Cell cell12 = new Cell { CellReference = "L" + nbRow, DataType = CellValues.String, CellValue = new CellValue(repo.NomOrdre) };
                                            Cell cell13 = new Cell { CellReference = "M" + nbRow, DataType = CellValues.String, CellValue = new CellValue(repo.LibRglt) };


                                            Cell cellF = new Cell() { CellReference = "AB" + nbRow, DataType = CellValues.Number };
                                            CellFormula cellformula = new CellFormula();
                                            cellformula.Text = "=H"+nbRow+"-SUM(R"+nbRow+":AA"+nbRow+")";
                                            CellValue cellValue = new CellValue();
                                            cellValue.Text = "0";
                                            cellF.Append(cellformula);
                                            cellF.Append(cellValue);


                                            // Append cell to row
                                            row.Append(cell1);
                                            row.Append(cell2);
                                            row.Append(cell3);
                                            row.Append(cell4);
                                            row.Append(cell5);
                                            row.Append(cell6);
                                            row.Append(cell7);
                                            row.Append(cell8);
                                            row.Append(cell9);
                                            row.Append(cell10);
                                            row.Append(cell11);
                                            row.Append(cell12);
                                            row.Append(cell13);
                                            row.Append(cellF);

                                            // Append row to sheetData
                                            sheetData.Append(row);

                                            nbRow++;
                                            nbEntree++;
                                            journal.Info("Insertion de la facture N° CHQ : " + repo.NumCheq);

                                        }

                                        log.Info("Ajout du ficher " + aFile + " à l'Excel");

                                        fileStream.Close();

                                        //déplace le fichier
                                        try
                                        {
                                            string nomFile = Path.GetFileNameWithoutExtension(aFile);
                                            string extensionFile = Path.GetExtension(aFile);

                                            nomFile = nomFile + dateString + extensionFile;

                                            System.IO.File.Move(aFile, pathRepoMovedFile + nomFile);
                                            log.Info("Déplacement du ficher " + aFile + " vers le dossier " + pathRepoMovedFile + nomFile);
                                        }
                                        catch (Exception e)
                                        {
                                            log.Error(e.Message);
                                        }
                                    }
                                    else
                                    {
                                        log.Error("Erreur lors de l'appel à l'API {code}", response.StatusCode);
                                    }
                                }
                                var rowsNew = sheet.Descendants<Row>();

                                uint nbRowNew = (uint)rows.LongCount();

                                uint diffRow = nbRowNew - nbRowBase;

                                journal.Info("Nombre de ligne envoyées : " + nbEntree + " | Nombre de ligne ajoutées au fichier : " + diffRow);

                                worksheetPart.Worksheet.Save();
                            }
                        }
                        catch (Exception e)
                        {
                            log.Fatal(e.Message);
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Fatal(e.Message);
                }
            }
            catch (Exception e)
            {
                log.Error("Accès échoué");
                log.Fatal(e.Message);
            }
        }

        private static async Task Main(string[] args)
        {
            //définit le httpClient avec les parametre pour le proxy OU pas si boolean proxy == false
            createHttpClient();

            copyExcel();

            await ProcessRepositories();
        }

        //récupère la bonne feuille du fichier excel (en fonction de son nom) et la retourne
        public static WorksheetPart GetWorksheetPartByName(WorkbookPart workbookPart, string sheetName)
        {
            string relId = workbookPart.Workbook.Descendants<Sheet>().First(s => sheetName.Equals(s.Name)).Id;
            return (WorksheetPart)workbookPart.GetPartById(relId);
        }

        //récupère les valeurs associées aux clés dans le fichier de config
        private static string ReadSetting(string key)
        {
            try
            {
                var appSettings = ConfigurationManager.AppSettings;
                string result = appSettings[key] ?? "Not Found";
                return result;
            }
            catch (ConfigurationErrorsException)
            {
                return "Error reading app settings";
            }
        }

        public static void createHttpClient()
        {
            if (useProxy == "True")
            {
                // create a proxy object
                var proxy = new WebProxy
                {
                    Address = new Uri($"http://{proxyHost}:{proxyPort}"),
                    BypassProxyOnLocal = false,
                    UseDefaultCredentials = false,

                    // *** These creds are given to the proxy server, not the web server ***
                    Credentials = new NetworkCredential(
                        userName: proxyUserName,
                        password: proxyPassword)
                };

                // create a client handler which uses that proxy
                var httpClientHandler = new HttpClientHandler
                {
                    //Proxy = proxy,
                    UseDefaultCredentials = true,
                };

                // Finally, create the HTTP client object
                client = new HttpClient(handler: httpClientHandler, disposeHandler: true);
            }
            else
            {
                client = new HttpClient();
            }
        }

        public static void copyExcel()
        {
            String dateString = getDate();

            pathExcelCopy = pathExcelCopy + nameExcelCopy + dateString + ".xlsx";
            try
            {
                log.Info("Tentative de copie du fichier Excel vers le repertoire " + pathExcelCopy);
                File.Copy(pathExcel, pathExcelCopy);
                log.Info("Copie réussie");
            }
            catch (Exception e)
             {
                log.Error("Copie échouée");
                log.Error(e);
             }
        }

        public static String getDate()
        {
            // Get the current date.
            DateTime thisDay = DateTime.Now;

            String day = thisDay.Day.ToString();
            if (day.Length == 1)
            {
                day = "0" + day;
            }

            String month = thisDay.Month.ToString();
            if (month.Length == 1)
            {
                month = "0" + month;
            }
            String year = thisDay.Year.ToString();

            String hour = thisDay.Hour.ToString();

            String min = thisDay.Minute.ToString();

            String dateString = "_" + year + "_" + month + "_" + day + "__" + hour + "_" + min;

            return dateString;
        }
    }
}
