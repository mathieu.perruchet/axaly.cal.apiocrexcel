﻿using System.Text.Json.Serialization;

namespace myApp
{
    public class Repository 
    {
        [JsonPropertyName("date de la pièce")]
        public string DatePiece { get; set; }

        [JsonPropertyName("Société")]
        public string Societe { get; set; }

        [JsonPropertyName("n° compte bancaire du bénéficiaire")]
        public string CompteBenef { get; set; }

        [JsonPropertyName("Mode de rglt")]
        public string ModeRglt { get; set; }

        [JsonPropertyName("date d'opération")]
        public string DateOp { get; set; }

        [JsonPropertyName("date de valeur")]
        public string DateValeur { get; set; }

        [JsonPropertyName("N° CHQ OU VIRM")]
        public string NumCheq { get; set; }

        [JsonPropertyName("Montant")]
        public string Montant { get; set; }

        [JsonPropertyName("Code banque")]
        public string Banque { get; set; }

        [JsonPropertyName("Code guichet")]
        public string Guichet { get; set; }

        [JsonPropertyName("Numero de compte")]
        public string Compte { get; set; }

        [JsonPropertyName("nom du donneur d'ordre")]
        public string NomOrdre { get; set; }

        [JsonPropertyName("Libellés des RGLT")]
        public string LibRglt { get; set; }
    }
}
